import findspark
findspark.init()
import pyspark
sc = pyspark.SparkContext(appName="Pi")
import happybase
import pandas as pd
import numpy as np
from Vectorisation import vectorise

connection = happybase.Connection('localhost',9090,autoconnect=False)
connection.open()
print(connection.tables(),"\n")
table = connection.table('test1')
count = 0
for _ in table.scan(filter='FirstKeyOnlyFilter() AND KeyOnlyFilter()'):
        count += 1

#a=[]
#b=[]
#for key, data in table.scan():
#    for key in data:
#     a.append(key)
#     b.append(data[key])
#    
##    print(a)
#
#    my_series = pd.Series(data)
#    print(my_series)

#b=[x.decode('utf-8') for x in b]
#a=[x.decode('utf-8') for x in a]
#
#vectorise(a)
#vectorise(b)
#print(table.row('1')) # {'f1:': 'hello'}
#table.put('5',{'c3:':'22'})
#table.delete(b'3')
df = pd.DataFrame()
for n in range(1,count+1):
    
    row=table.row(row=str(n))
    temp=[]
    for key,value in row.items():
        temp.append(value)
    temp=[x.decode('utf-8') for x in temp]
    df = df.append(pd.DataFrame([temp],columns=['firstname','lastname','age']),ignore_index=True)

    vectorise(temp)
print(df)
