# -*- coding: utf-8 -*-
from nltk.tokenize import word_tokenize
#using gensim.models.word2vec package

from word2vec import feature2vec

def vectorise(_list):
    feature=[]
    
    if _list==[' ']:
        return [0]
    else:
        for text in _list:
            feature.append(word_tokenize(text))
    
        feature2vector=feature2vec(feature)
        words=[]
        
        words=list(feature2vector.wv.vocab)
#        print(words)
        vectors=[]
        
        vectors=list(feature2vector[words])
        my_list = map(lambda x: x[0], vectors)
        ser = list(my_list)
        
        return ser


    
