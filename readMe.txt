

To run hbase connection with spark use, more jars needed to be added

export SPARK_CLASSPATH=$HBASE_HOME/lib/hbase-common-1.0.0-cdh5.4.1-tests.jar:$HBASE_HOME/lib/hbase-client-1.0.0-cdh5.4.1.jar:$HBASE_HOME/lib/hbase-server-1.0.0-cdh5.4.1.jar:$HBASE_HOME/lib/hbase-protocol-1.0.0-cdh5.4.1.jar:$HBASE_HOME/lib/guava-12.0.1.jar:$HBASE_HOMElib/htrace-core-3.1.0-incubating.jar:$SPARK_HOME/conf/     -->to be added in .bashrc

Run code in terminal:

You should have the input table in Hbase table with the following schema:

column family: duplicate_check  --> columns: id, name, description, doc_attached 

cd /PROJECT_DIRECTORY

spark-submit --jars /home/gofreelab/cdit/analytical-engine/analytical-engine/shc/core/target/shc-core-1.1.3-2.4-s_2.11.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-client-2.1.4.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-common-2.1.4.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-shaded-miscellaneous-2.1.0.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-protocol-shaded-2.1.4.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-shaded-protobuf-2.1.0.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-protocol-2.1.4.jar,/home/gofreelab/hbase-2.1.4/lib/hbase-shaded-netty-2.1.0.jar,/home/gofreelab/hbase-2.1.4/lib/client-facing-thirdparty/htrace-core4-4.2.0-incubating.jar,/home/gofreelab/hbase-2.1.4/lib/shaded-clients/hbase-shaded-mapreduce-2.1.4.jar  --files /home/gofreelab/hbase-2.1.4/conf/hbase-site.xml /pyspark-hbase.py

./run.sh duplicateMain.py
