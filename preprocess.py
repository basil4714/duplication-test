def stop_word(str_list): 
 list =["അതിൽ","ഇതിൽ","ഇത്","ആണ്","അത്","എന്ന","ഈ","ആ","\n"]
 another_list = []
 for x in str_list:
    if x not in list:           
        another_list.append(x) 
 return another_list



def cleantext(strList):
    
 punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
 
 no_punct = ""
 for char in strList:
   if char not in punctuations:
       no_punct = no_punct + char

 userstring = no_punct.split(" ")
 if userstring==['', '']:
     return [' ']
 else:
     return userstring