from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.session import SparkSession
import base64
from preprocess import stop_word,cleantext
from Vectorisation import vectorise
from cosine import compare
import pandas as pd
from datetime import date
import ast


sc = SparkContext()
sqlc = SQLContext(sc)

data_source_format = 'org.apache.spark.sql.execution.datasources.hbase'


catalog = ''.join("""{
"table":{
    "namespace":"default",
    "name":"CMDRF"
    },
"rowkey":"key",
"columns":{
    "rowkey":{"cf":"rowkey", "col":"key", "type":"string"},
    "id":{"cf":"duplicate_check","col":"id","type":"string"},
    "name":{"cf":"duplicate_check","col":"name","type":"string"},
    "description":{"cf":"duplicate_check","col":"description","type":"string"},
    "doc_attached":{"cf":"duplicate_check","col":"doc_attached","type":"string"}
}
}""".split())

 # Reading
df = sqlc.read\
.options(catalog=catalog)\
.format(data_source_format)\
.load()
dfAfterDrop=df.drop("rowkey")
dfAfterDrop.show()

def df_to_list(x):
    y=list(x)
    return y

df_to_list_out=dfAfterDrop.rdd.map(df_to_list).collect()


decodeInput= sc.parallelize(df_to_list_out)
def decodebase64(df_to_list_out):
        
        a=df_to_list_out[3]
    
        b=base64.b64decode(a)
        decoded=str(b, 'utf-8')
        df_to_list_out[3]=decoded    #df_to_list_out is updated
        return df_to_list_out

decodeOut = decodeInput.map(lambda x:decodebase64(x)).collect()
no_of_columns=len(dfAfterDrop.columns)
no_rows=dfAfterDrop.count()



cleaned_list=[]
for i in range(no_rows):
    oneList = decodeOut[i]
    tempList=[]
    for j in range(no_of_columns):
        input_for_cleantext=sc.parallelize([oneList[j]])
        cleaned = input_for_cleantext.map(cleantext).collect()
        tempList.append(cleaned[0])
    cleaned_list.append(tempList)



stopsRemoved=[]
for i in range(no_rows):
    oneList = cleaned_list[i]
    templist=[]
    for j in range(no_of_columns):
        input_for_stopwords=sc.parallelize([oneList[j]])
        stopwordsOut = input_for_stopwords.map(stop_word).collect()
        templist.append(stopwordsOut[0])
    stopsRemoved.append(templist)


def vector(stopsRemoved):
    vec_list=[]    #vectorising indv_list 
    for i in range(no_rows):
        oneList = stopsRemoved[i]
        tempList=[]
        for j in range(no_of_columns):
            
            vectorised= vectorise(oneList[j])
            tempList.append(vectorised)
        vec_list.append(tempList)
    return vec_list
vec_list=sc.parallelize([stopsRemoved]).map(vector).collect() 
vec_list=vec_list[0]


def sim_fields(x,y):        #function obtaining identical fields
            identical_fields=[]
            for q in (x):
                identical_fields.append(y[q])
            return identical_fields

catalog2 = ''.join("""{
"table":{
    "namespace":"default",
    "name":"tbl_duplicates"
    },
"rowkey":"key",
"columns":{
    "rowkey":{"cf":"rowkey", "col":"key", "type":"string"},
    "date_Compared":{"cf":"duplicate_check","col":"date_Compared","type":"string"},
    "source_Id":{"cf":"duplicate_check","col":"source_Id","type":"string"},
    "Duplicates_Id_ratio":{"cf":"duplicate_check","col":"Duplicates_Id_ratio","type":"string"}
}
}""".split())
finalHbase_df1 = sqlc.read\
.options(catalog=catalog2)\
.format(data_source_format)\
.load()
finalHbase_df1=finalHbase_df1.drop('rowkey')
final=finalHbase_df1.rdd.map(df_to_list).collect()
for i in final:
    i[2]=ast.literal_eval(i[2])


date_compared = date.today()
for i in range(no_rows):
    find_idi=df_to_list_out[i]
    
   
    for j in range(no_rows):
        duplicates=[date_compared,find_idi[0],0]

        find_idj=df_to_list_out[j]
        if(j==i):
            continue
        row1=list(vec_list[i])
        row2=list(vec_list[j])
        club=[row1,row2]
        club=sc.parallelize(club)
        sim_ratio=club.reduce(compare)
        f=dfAfterDrop.columns
        
        if(sim_ratio==None):
          continue
        
        sim_fields_input=[sim_ratio[1],f]
        sim_fields_input_rdd=sc.parallelize(sim_fields_input)
        identical_fields=sim_fields_input_rdd.reduce(sim_fields)
        duplicates[2]=[find_idj[0],sim_ratio[0],identical_fields]
    
    
        final.append(duplicates)


def rmv_repetition(final):
    ff=[]
    tt=[]
    rowkey = 0
    for i in range(len(final)):
            
            single=final[i]
            b=set([single[1],single[2][0]])
            if b not in tt:
                
                tt.append(b)
                rowkey +=1
                single.insert(0,str(rowkey))
                ff.append(single)
    return ff
    
final_rdd=sc.parallelize([final])
final=final_rdd.map(rmv_repetition).collect()  

def convertList2Str(inLists):
    strings=[str(i) for i in inLists]
    return strings

final=sc.parallelize(final[0]).map(convertList2Str).collect()
final_df1=pd.DataFrame(final)



spark = SparkSession(sc)
finalHbase_df1=spark.createDataFrame(final_df1,schema=['rowkey','date_Compared','source_Id','Duplicates_Id_ratio'])
finalHbase_df1.show()

   # Writing

finalHbase_df1.write.options(catalog=catalog2).format(data_source_format).save()

finalHbase_df1 = sqlc.read\
.options(catalog=catalog2)\
.format(data_source_format)\
.load()

dfAfterDrop.show()

finalHbase_df1.show()

dist_src_id=finalHbase_df1.select('source_Id').distinct()
dist_src_id=dist_src_id.rdd.map(df_to_list).collect()


def final_List(dist_src_id):
    print("dddd",dist_src_id)
    dist_src_Id=[]
    for i in range(len(dist_src_id)):
        dist_src_Id.append(dist_src_id[i][0])

    final1 = []
    compared=[]

    for i in range(len(dist_src_Id)):
        if(dist_src_Id[i]) not in compared:
            compared.append(dist_src_Id[i])

            a = finalHbase_df1.select('Duplicates_Id_ratio').filter(finalHbase_df1.source_Id == dist_src_Id[i])
            final1.append(dist_src_Id[i])
            single1=[]

            for r in range(len(a.toPandas()['Duplicates_Id_ratio'])):
                single1.append(ast.literal_eval(a.toPandas()['Duplicates_Id_ratio'][r]))
            final1.append(single1)
            
            for j in single1:
                    compared.append(j[0])
    return final1

final1=sc.parallelize([dist_src_id]).map(final_List).collect()
#print(final1)

#dist_src_Id=[]
#for i in range(len(dist_src_id)):
#    dist_src_Id.append(dist_src_id[i][0])
#
#final1 = []
#compared=[]
#rowkey=0
#for i in range(len(dist_src_Id)):
#        
#        if(dist_src_Id[i]) not in compared:
#            compared.append(dist_src_Id[i])
#
#            a = finalHbase_df1.select('Duplicates_Id_ratio').filter(finalHbase_df1.source_Id == dist_src_Id[i])
#            single1=[]
#            duplicates=[]
#            duplicates.append(dist_src_Id[i])
#
#            for r in range(len(a.toPandas()['Duplicates_Id_ratio'])):
#                single1.append(ast.literal_eval(a.toPandas()['Duplicates_Id_ratio'][r]))
#            duplicates.append(single1)
#            rowkey +=1
#            duplicates.insert(0,str(rowkey))
#            final1.append(duplicates)
#            
#            for j in single1:
#                    compared.append(j[0])

catalog3 = ''.join("""{
"table":{
    "namespace":"default",
    "name":"duplicates"
    },
"rowkey":"key",
"columns":{
    "rowkey":{"cf":"rowkey", "col":"key", "type":"string"},
    "source_Id":{"cf":"duplicate_check","col":"source_Id","type":"string"},
    "Duplicates_Id_ratio":{"cf":"duplicate_check","col":"Duplicates_Id_ratio","type":"string"}
}
}""".split())
    
final1=sc.parallelize(final1).map(convertList2Str).collect()
final_df2=pd.DataFrame(final1)
finalHbase_df2=spark.createDataFrame(final_df2,schema=['rowkey','source_Id','Duplicates_Id_ratio'])

    # Writing

finalHbase_df2.write.options(catalog=catalog3).format(data_source_format).save()

finalHbase_df2 = sqlc.read\
.options(catalog=catalog3)\
.format(data_source_format)\
.load()
finalHbase_df2.show()

    




    

    